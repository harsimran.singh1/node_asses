const mysql = require("mysql");
const { mySql_password } = require("./secret");

const db = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: `${mySql_password}`,
    database: "catsdb"
})

db.connect((err) => {
    if (err) throw err;
    console.log("MySql connected");
})

module.exports = db;